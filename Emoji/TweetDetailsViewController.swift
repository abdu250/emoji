//
//  TweetDetailsViewController.swift
//  Emoji
//
//  Created by Abdullah Al-Ahmadi on 1/28/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit

class TweetDetailsViewController: UIViewController {

    var tweet = ""
  
  @IBOutlet weak var tweetTitle: UILabel!
  @IBOutlet weak var tweetText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      tweetTitle.text = tweet
      if tweetTitle.text == "Abdullah" {
        tweetText.text = "The full name is Abdullah Saad Mohammed Al-Ahmadi"
      }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
