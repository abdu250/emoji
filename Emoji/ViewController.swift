//
//  ViewController.swift
//  Emoji
//
//  Created by Abdullah Al-Ahmadi on 1/28/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

  @IBOutlet weak var listOfTweets: UITableView!
  var tweets = ["Welcome", "Abdullah", "Saad", "Al-Ahmadi"]
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    listOfTweets.dataSource = self
    listOfTweets.delegate = self
    
    
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return tweets.count
    
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    cell.textLabel?.text = tweets[indexPath.row]
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated:true)
    let tweet = tweets[indexPath.row]
    performSegue(withIdentifier: "moveToTweetDetails", sender: tweet)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let desTweetDetailController = segue.destination as! TweetDetailsViewController
    desTweetDetailController.tweet = sender as! String
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

